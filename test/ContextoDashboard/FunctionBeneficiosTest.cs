using System;
using Amazon.Lambda.APIGatewayEvents;
using Xunit;
using Amazon.Lambda.TestUtilities;
using maisfidelidade.veiculos.backoffice.relatorioresgates;

namespace maisfidelidade.veiculos.backoffice.api.dashboard.Tests
{
    public class FunctionTest
    {
        [Fact]
        public void TestToUpperFunction()
        {

            Environment.SetEnvironmentVariable("ASPNETCORE_ENVIRONMENT", "dev");
            var function = new Function();
            var context = new TestLambdaContext
            {
                InvokedFunctionArn = ":dev"
            };

            APIGatewayProxyRequest request = new APIGatewayProxyRequest();

            var result = function.FunctionHandler(request, context);

            Assert.NotEmpty(result.Body);

        }
    }
}
