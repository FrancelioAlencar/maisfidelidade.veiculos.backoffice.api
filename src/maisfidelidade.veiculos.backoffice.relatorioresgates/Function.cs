using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Amazon.Lambda.APIGatewayEvents;
using Amazon.Lambda.Core;
using maisfidelidade.veiculos.backoffice.core.DI;
using maisfidelidade.veiculos.backoffice.core.Dominio;
using Newtonsoft.Json;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace maisfidelidade.veiculos.backoffice.relatorioresgates
{
    public class Function
    {
        private readonly IResgateRepositorio resgateRepositorio;

        public Function()
        {
            var resolvedorDependencia = new ResolvedorDependencia();

            resgateRepositorio = resolvedorDependencia.ObterServico<IResgateRepositorio>();
        }


        /// <summary>
        /// A simple function that takes a string and does a ToUpper
        /// </summary>
        /// <param name="input"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public APIGatewayProxyResponse FunctionHandler(APIGatewayProxyRequest request, ILambdaContext context)
        {
            try
            {
                var resgates = resgateRepositorio.ObterUltimosResgates();

                var mesAtual = resgates.OrderByDescending(r => r.DataReferencia).FirstOrDefault();
                var historicoMensal = resgates?.Where(r => (r.Mes == mesAtual.Mes && r.Ano == mesAtual.Ano) == false).OrderByDescending(r => r.DataReferencia);

                var relatorioResgate = new RelatorioResgate
                {
                    MesAtual = mesAtual,
                    HistorioMensal = new List<RelatorioResgateItem>(historicoMensal)
                };

                return new APIGatewayProxyResponse()
                {
                    StatusCode = (int)HttpStatusCode.OK,
                    Headers = new Dictionary<string, string> { { "Access-Control-Allow-Origin", "*" }, { "Access-Control-Allow-Headers", "*" } },
                    Body = JsonConvert.SerializeObject(relatorioResgate, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore, MissingMemberHandling = MissingMemberHandling.Ignore })
                };
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
    }
}
