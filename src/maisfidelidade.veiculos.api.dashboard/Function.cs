using Amazon.Lambda.APIGatewayEvents;
using Amazon.Lambda.Core;
using maisfidelidade.veiculos.backoffice.core.DI;
using maisfidelidade.veiculos.backoffice.core.Dominio;
using maisfidelidade.veiculos.backoffice.core.Dominio.Importacao;
using maisfidelidade.veiculos.backoffice.core.Dominio.Dashboard;
using maisfidelidade.veiculos.backoffice.core.Infrastructure.Repositorios;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using MongoDB.Bson;

[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace maisfidelidade.veiculos.backoffice.api.dashboard
{
    public class Function
    {
        private readonly IArquivoRepositorio arquivoRepositorio;
        private readonly ILogMongoRepositorio logMongoRepositorio;

        public Function()
        {
            var resolvedorDependencia = new ResolvedorDependencia();

            arquivoRepositorio = resolvedorDependencia.ObterServico<IArquivoRepositorio>();
            logMongoRepositorio = resolvedorDependencia.ObterServico<ILogMongoRepositorio>();
        }

        public APIGatewayProxyResponse FunctionHandler(APIGatewayProxyRequest request, ILambdaContext _lambdaContext)
        {
            try
            {
                var arquivos = arquivoRepositorio.Obter();

                var dashboard = new DashboardCompleto
                {
                    Mes = DateTime.Now,
                    MesExtenso = DateTime.Now.ToString("MMMM"),
                    Itens = new List<DashboardCardItem>(),
                    UltimosExtratos = new List<DashboardUltimosExtratos>()
                };

                int registrosProcessados = 0, arquivosProcessados = 0, registrosComErros = 0;

                DateTime hoje = DateTime.Now;
                DateTime primeiroDiaDoMes = new DateTime(hoje.Year, hoje.Month, 1);
                DateTime ultimoDiaDoMes = new DateTime(hoje.Year, hoje.Month, DateTime.DaysInMonth(hoje.Year, hoje.Month));

                var listaArquivos = arquivos.Where(x => x.dt_referencia >= primeiroDiaDoMes && x.dt_referencia <= ultimoDiaDoMes);

                //foreach (var arquivo in arquivos.GroupBy(x => x.dt_referencia.ToString("MM/yyyy")))
                //{
                    registrosProcessados = listaArquivos.Sum(x => x.qtd_linhas);
                    arquivosProcessados = listaArquivos.Count();

                    int erros = 0;

                    foreach (var item in listaArquivos)
                    {
                        var log = logMongoRepositorio.Obter(new ObjectId(item.object_id));

                        var logErros = log.Where(x => x.Tipo == core.Log.TipoLog.Falha).GroupBy(x => x.StatusLog).ToList();

                        erros += logErros.Count;

                        dashboard.UltimosExtratos.Add(new DashboardUltimosExtratos
                        {
                            DataReferencia = item.dt_referencia,
                            Data = item.dt_referencia.ToString("dd/MMM"),
                            IdArquivo = item.object_id,
                            LinkArquivo = item.link_arquivo,
                            LinkExtrato = item.link_extrato,
                            NomeArquivo = item.nome,
                            QuantidadeLinhas = item.qtd_linhas,
                            TipoArquivo = (TipoArquivoEnum)Enum.Parse(typeof(TipoArquivoEnum), item.tipo_arquivo.ToString()),
                            Publicado = true
                        });
                    }

                    registrosComErros = erros;

                    //break;
                //}

                var cards = new List<DashboardCardItem>();

                cards.Add(new DashboardCardItem
                {
                    Tipo = TipoCardEnum.RegistrosProcessados,
                    Quantidade = registrosProcessados,
                    Mensagem = "Registros processados no m�s."
                });

                cards.Add(new DashboardCardItem
                {
                    Tipo = TipoCardEnum.ArquivosProcessados,
                    Quantidade = arquivosProcessados,
                    Mensagem = "Arquivos processados no m�s."
                });                

                cards.Add(new DashboardCardItem
                {
                    Tipo = TipoCardEnum.RegistrosComErros,
                    Quantidade = registrosComErros,
                    Mensagem = "Erros encontrados nos arquivos enviados."//registros processados."
                });

                dashboard.Itens = cards;

                return new APIGatewayProxyResponse()
                {
                    StatusCode = (int)HttpStatusCode.OK,
                    Headers = new Dictionary<string, string> { { "Access-Control-Allow-Origin", "*" }, { "Access-Control-Allow-Headers", "*" } },
                    Body = JsonConvert.SerializeObject(dashboard, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore, MissingMemberHandling = MissingMemberHandling.Ignore })
                };
            }
            catch (Exception _exception)
            {
                _exception.Data.Add("LambdaContext", JsonConvert.SerializeObject(_lambdaContext));
                return null;
            }
        }
    }
}
