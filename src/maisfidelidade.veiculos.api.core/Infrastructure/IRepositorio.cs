﻿using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.veiculos.backoffice.core.Infrastructure
{
    public interface IRepositorio<TEntityAggregate>
    {
        TEntityAggregate BuscarPor(int id);
        IList<TEntityAggregate> BuscarTodos();
    }
}
