﻿using MySql.Data.MySqlClient;

namespace maisfidelidade.veiculos.backoffice.core.Infrastructure.Data.MySql
{
    public class MySqlDatabase : Database
    {
        public MySqlDatabase(string connectionString)
            : base(connectionString, MySqlClientFactory.Instance)
        {

        }        
    }
}
