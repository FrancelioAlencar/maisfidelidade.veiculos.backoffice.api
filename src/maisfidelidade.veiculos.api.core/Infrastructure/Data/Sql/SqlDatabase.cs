﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace maisfidelidade.veiculos.backoffice.core.Infrastructure.Data.Sql
{
    public class SqlDatabase : Database
    {
        public SqlDatabase(string connectionString)
            : base(connectionString, SqlClientFactory.Instance)
        {

        }
    }
}
