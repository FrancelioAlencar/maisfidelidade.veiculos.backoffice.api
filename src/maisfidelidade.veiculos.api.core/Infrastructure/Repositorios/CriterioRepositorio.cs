﻿using maisfidelidade.veiculos.backoffice.core.Dominio;
using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.veiculos.backoffice.core.Infrastructure.Repositorios
{
    public class CriterioRepositorio : MySqlRepositorioBase<CriterioCliente>, ICriterioRepositorio
    {
        public IList<CriterioCliente> BuscarCriterioClientePor(int idCliente)
        {
            IList<CriterioCliente> criteriosCliente = new List<CriterioCliente>();

            StringBuilder builder = new StringBuilder();

            builder.Append("SELECT c.id_entidade AS Id, c.id_criterio AS IdCriterio, c.nome AS Nome, c.descricao AS Descricao, c.id_empresa AS IdEmpresa, c.id_tipo_criterio AS IdTipoCriterio,");
            builder.Append(" apc.pontos AS PontuacaoAtual, apc.porcentagem AS PorcentagemAtual, rc.quantidade_pontos AS PontuacaoNecessaria, rc.porcentagem_atingimento AS PorcentagemNecessaria FROM autos_tb_criterio c");
            builder.Append(" INNER JOIN autos_tb_apuracao_criterio apc ON apc.id_criterio = c.id_criterio");
            builder.Append(" INNER JOIN autos_tb_apuracao ap ON ap.id_apuracao = apc.id_apuracao");
            builder.Append(" INNER JOIN autos_tb_arquivo arq ON arq.id_arquivo = ap.id_arquivo");
            builder.Append(" INNER JOIN autos_tb_regra_criterio rc ON rc.id_criterio = c.id_criterio");
            builder.Append(" INNER JOIN autos_tb_criterio_regra_subsegmento crs ON crs.id_regra_criterio = rc.id_regra_criterio AND crs.id_segmento = ap.id_segmento");
            builder.Append($" WHERE c.ativo = 1 AND arq.importado = 1 AND arq.ultimo_processado = 1 AND arq.referencia_arquivo = 'S' AND ap.id_cliente = {idCliente}");
            builder.Append(" ORDER BY c.id_empresa DESC, c.ordem_visualizacao ASC;");
                                   
            return this.BuildEntitiesFromSql(builder.ToString());
        }

        protected override void BuildChildCallbacks()
        {
        }

        protected override string GetBaseQuery()
        {
            return "SELECT c.id_criterio AS Id, c.nome AS Nome, c.descricao AS Descricao, c.id_empresa AS IdEmpresa, c.id_tipo_criterio AS IdTipoCriterio FROM autos_tb_criterio c" +
                " INNER JOIN autos_tb_apuracao_criterio apc ON apc.id_criterio = c.id_criterio" +
                " INNER JOIN autos_tb_apuracao ap ON ap.id_apuracao = apc.id_apuracao" +
                " INNER JOIN autos_tb_arquivo arq ON arq.id_arquivo";
        }

        protected override string GetBaseWhereClause()
        {
            return " WHERE c.ativo = 1;";
        }
    }
}
