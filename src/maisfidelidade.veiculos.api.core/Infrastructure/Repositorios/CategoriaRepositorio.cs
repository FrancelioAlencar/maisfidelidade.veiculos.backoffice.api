﻿using maisfidelidade.veiculos.backoffice.core.Dominio;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace maisfidelidade.veiculos.backoffice.core.Infrastructure.Repositorios
{
    public class CategoriaRepositorio : MySqlRepositorioBase<Categoria>, ICategoriaRepositorio
    {
        public CategoriaRepositorio()
        {

        }

        public IList<BeneficioCategoria> BuscarBeneficiosCategoriaCustomizadosPor(int idCliente)
        {
            IList<BeneficioCategoria> beneficiosCategoria = new List<BeneficioCategoria>();

            StringBuilder builder = new StringBuilder();
            builder.Append("SELECT b.id_beneficio AS IdBeneficio, b.nome AS Nome, bc.Mensagem AS Descricao, bc.id_categoria AS IdCategoria FROM autos_tb_beneficio_categoria bc");
            builder.Append(" INNER JOIN autos_tb_beneficio b ON b.id_beneficio = bc.id_beneficio");
            builder.Append(" INNER JOIN autos_tb_categoria c ON c.id_categoria = bc.id_categoria");
            builder.Append(" WHERE bc.ativo = 1 AND b.ativo = 1 AND c.ativo = 1");
            builder.Append($" AND bc.id_beneficio_categoria IN (SELECT id_beneficio_categoria FROM tb_beneficio_regra_cliente WHERE id_cliente = {idCliente});");

            beneficiosCategoria = this.ExecuteQuery<BeneficioCategoria>(builder.ToString());

            return beneficiosCategoria;
        }

        public override IList<Categoria> BuscarTodos()
        {
            StringBuilder builder = this.GetBaseQueryBuilder();
            builder.Append(this.GetBaseWhereClause());

            return this.BuildEntitiesFromSql(builder.ToString());
        }

        #region BuildChildCallbacks

        protected override void BuildChildCallbacks()
        {
            this.ChildCallbacks.Add("registros",
                delegate (IList<Categoria> categorias, object childKeyName)
                {
                    this.AppendBeneficios(categorias);
                });
        }
        
        #endregion

        #region GetBaseQuery

        protected override string GetBaseQuery()
        {
            return "SELECT id_categoria AS Id, nome AS Nome FROM autos_tb_categoria";
        }

        #endregion

        protected override string GetBaseWhereClause()
        {
            return " WHERE ativo = 1";
        }

        #region Métodos de Callback e Auxiliares Privados

        private void AppendBeneficios(IList<Categoria> categorias)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("SELECT b.id_beneficio AS IdBeneficio, b.nome AS Nome, bc.Mensagem AS Descricao, bc.id_categoria AS IdCategoria FROM autos_tb_beneficio_categoria bc");
            builder.Append(" INNER JOIN autos_tb_beneficio b ON b.id_beneficio = bc.id_beneficio");
            builder.Append(" INNER JOIN autos_tb_categoria c ON c.id_categoria = bc.id_categoria");
            builder.Append(string.Format(" WHERE bc.ativo = 1 AND b.ativo = 1 AND c.ativo = 1 AND bc.fl_regra = 0;"));

            var beneficios = this.ExecuteQuery<BeneficioCategoria>(builder.ToString());

            foreach (var categoria in categorias)
            {
                categoria.Beneficios = beneficios.Where(b => b.IdCategoria == categoria.Id).ToList();
            }
        }



        #endregion
    }
}
