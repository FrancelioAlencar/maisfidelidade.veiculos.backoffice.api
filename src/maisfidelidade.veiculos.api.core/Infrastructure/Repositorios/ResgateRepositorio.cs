﻿using maisfidelidade.veiculos.backoffice.core.Dominio;
using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.veiculos.backoffice.core.Infrastructure.Repositorios
{
    public class ResgateRepositorio : MySqlRepositorioBase<RelatorioResgate>, IResgateRepositorio
    {
        public ResgateRepositorio()
        {

        }

        public IList<RelatorioResgateItem> ObterUltimosResgates()
        {
            StringBuilder builder = new StringBuilder();

            builder.Append("SELECT arq.id_arquivo AS IdArquivo, arq.dt_referencia AS DataReferencia, MONTH(arq.dt_referencia) AS Mes, YEAR(arq.dt_referencia) AS Ano,");
            builder.Append(" CASE WHEN (SELECT COUNT(*) FROM autos_tb_beneficio_resgate WHERE MONTH(dt_inclusao) = Mes AND YEAR(dt_inclusao) = Ano AND fl_disponivel_resgate = 1) > 0 THEN true ELSE false END AS Liberado FROM autos_tb_arquivo arq");
            builder.Append(" WHERE arq.dt_referencia > ADDDATE(cast(concat(LAST_DAY(DATE_SUB(NOW(), INTERVAL 4 MONTH)), ' ', '00:00:00') as datetime), 1)");
            builder.Append(" AND arq.tipo_arquivo = 2 AND arq.id_status_processamento = 7 ");
            builder.Append(" ORDER BY Ano DESC, Mes DESC;");

            return this.ExecuteQuery<RelatorioResgateItem>(builder.ToString());
        }

        public IList<BeneficioResgatado> ObterResgate(int IdArquivo, DateTime dataInicio, DateTime dataFim)
        {
            StringBuilder builder = new StringBuilder();

            builder.Append("SELECT par.cnpj AS CNPJ, cat.nome AS Categoria, ben.nome AS Beneficio, br.porcentagem AS Porcentagem,");
            builder.Append(" br.dt_inclusao AS DataInclusao, br.fl_acao_seja_platinum AS AcaoSejaPlatinum FROM autos_tb_beneficio_resgate br");
            builder.Append(" INNER JOIN autos_tb_beneficio ben ON ben.id_beneficio = br.id_beneficio");
            builder.Append(" INNER JOIN autos_tb_apuracao apu ON apu.id_cliente = br.id_cliente");
            builder.Append(" INNER JOIN autos_tb_categoria cat ON cat.id_categoria = apu.id_categoria");
            builder.Append(" INNER JOIN vmotor_BDV1.PARTNERS par ON par.id = br.id_cliente");
            builder.Append($" WHERE br.dt_inclusao BETWEEN '{dataInicio.ToString("yyy-MM-dd HH:mm:ss")}' AND '{dataFim.ToString("yyy-MM-dd HH:mm:ss")}'");
            builder.Append($" AND apu.id_arquivo = {IdArquivo} AND br.fl_resgate = 1");
            builder.Append(" ORDER BY par.cnpj, br.dt_inclusao ASC;");

            return this.ExecuteQuery<BeneficioResgatado>(builder.ToString());
        }

        protected override void BuildChildCallbacks()
        {
            
        }

        protected override string GetBaseQuery()
        {
            return "";
        }

        protected override string GetBaseWhereClause()
        {
            return "";
        }
    }
}
