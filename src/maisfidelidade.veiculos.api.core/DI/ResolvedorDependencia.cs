using maisfidelidade.veiculos.backoffice.core.Dominio;
using maisfidelidade.veiculos.backoffice.core.Dominio.Importacao;
using maisfidelidade.veiculos.backoffice.core.Infrastructure;
using maisfidelidade.veiculos.backoffice.core.Infrastructure.Repositorios;
using maisfidelidade.veiculos.backoffice.core.Infrastructure.Repositorios.MongoDB;
using maisfidelidade.veiculos.backoffice.core.Log;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace maisfidelidade.veiculos.backoffice.core.DI
{
    public class ResolvedorDependencia
    {
        private readonly IServiceProvider provedorServico;

        public ResolvedorDependencia()
        {
            var serviceCollection = new ServiceCollection();

            AdicionarVinculos(serviceCollection);

            provedorServico = serviceCollection.BuildServiceProvider();
        }

        public T ObterServico<T>()
        {
            try
            {
                return provedorServico.GetService<T>();
            }
            catch  (Exception exception)
            {
                throw new Exception("", exception);
            }
        }

        public object ObterServico(Type servicoType)
        {
            try
            {
                return provedorServico.GetService(servicoType);
            }
            catch (Exception exception)
            {
                throw new Exception("", exception);
            }
        }
        
        private void AdicionarVinculos(IServiceCollection servicos)
        {
            servicos.AddTransient<ICategoriaRepositorio, CategoriaRepositorio>();
            servicos.AddTransient<ICriterioRepositorio, CriterioRepositorio>();
            servicos.AddTransient<IResgateRepositorio, ResgateRepositorio>();
            servicos.AddTransient<IArquivoRepositorio, ArquivoRepositorio>();
            servicos.AddTransient<IArquivoMongoRepositorio, ArquivoMongoRepositorio>();
            servicos.AddTransient<ILogMongoRepositorio, LogMongoRepositorio>();
        }
    }
}
