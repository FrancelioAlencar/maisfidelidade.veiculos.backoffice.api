﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.veiculos.backoffice.core.Dominio
{
    public class BeneficioCategoria
    {
        [JsonProperty("id_beneficio")]
        public int IdBeneficio { get; set; }

        [JsonProperty("name")]
        public string Nome { get; set; }

        [JsonProperty("description")]
        public string Descricao { get; set; }

        [JsonProperty("id_categoria")]
        public int IdCategoria { get; set; }
    }
}
