﻿using maisfidelidade.veiculos.backoffice.core.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.veiculos.backoffice.core.Dominio
{
    public interface ICategoriaRepositorio : IRepositorio<Categoria>
    {
        IList<BeneficioCategoria> BuscarBeneficiosCategoriaCustomizadosPor(int idCliente);
    }
}
