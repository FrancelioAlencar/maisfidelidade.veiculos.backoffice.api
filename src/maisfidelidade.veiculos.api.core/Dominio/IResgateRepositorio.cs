﻿using maisfidelidade.veiculos.backoffice.core.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.veiculos.backoffice.core.Dominio
{
    public interface IResgateRepositorio : IRepositorio<RelatorioResgate>
    {
        IList<RelatorioResgateItem> ObterUltimosResgates();
        IList<BeneficioResgatado> ObterResgate(int IdArquivo, DateTime dataInicio, DateTime dataFim);
    }
}
