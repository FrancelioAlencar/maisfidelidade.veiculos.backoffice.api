﻿using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.veiculos.backoffice.core.Dominio
{
    [BsonIgnoreExtraElements]
    public class StatusArquivoMessagem
    {
        [BsonElement("mensagem")]
        [JsonProperty("message")]
        public string Mensagem { get; set; }
    }
}
