﻿using maisfidelidade.veiculos.backoffice.core.Dominio.Enums;
using maisfidelidade.veiculos.backoffice.core.Infrastructure;
using maisfidelidade.veiculos.backoffice.core.Log;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.veiculos.backoffice.core.Dominio
{
    public interface ILogMongoRepositorio : IMongoRepositorio<LogInfo>
    {
        ObjectId? Salvar(LogInfo entidade);
        IList<LogInfo> Obter(ObjectId idArquivo);
    }
}
