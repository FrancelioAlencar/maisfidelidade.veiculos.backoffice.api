﻿using maisfidelidade.veiculos.backoffice.core.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.veiculos.backoffice.core.Dominio
{
    public interface ICriterioRepositorio : IRepositorio<CriterioCliente>
    {
        IList<CriterioCliente> BuscarCriterioClientePor(int idCliente);
    }
}
