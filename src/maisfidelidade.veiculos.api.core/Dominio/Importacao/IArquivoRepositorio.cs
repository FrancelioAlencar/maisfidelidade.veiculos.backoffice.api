﻿using maisfidelidade.veiculos.backoffice.core.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.veiculos.backoffice.core.Dominio.Importacao
{
    public interface IArquivoRepositorio : IRepositorio<Arquivo>
    {
        IList<Arquivo> Obter();
        Arquivo Obter(int idArquivo);
    }
}