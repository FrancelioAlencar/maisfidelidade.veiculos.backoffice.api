﻿using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.veiculos.backoffice.core.Dominio.Importacao
{
	public enum TipoArquivoEnum : int
	{
		Semanal = 1,
		Mensal = 2
	}
}
