﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.veiculos.backoffice.core.Dominio
{
    public class RelatorioResgate
    {
        public RelatorioResgate()
        {
            HistorioMensal = new List<RelatorioResgateItem>();
        }

        [JsonProperty("current_month")]
        public RelatorioResgateItem MesAtual { get; set; }

        [JsonProperty("monthly_history")]
        public IList<RelatorioResgateItem> HistorioMensal { get; set; }
    }
}
