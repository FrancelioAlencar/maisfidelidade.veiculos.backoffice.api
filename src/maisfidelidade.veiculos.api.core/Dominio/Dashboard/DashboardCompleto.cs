﻿using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.veiculos.backoffice.core.Dominio.Dashboard
{
    public class DashboardCompleto
    {
        public DateTime Mes { get; set; }
        public string MesExtenso { get; set; }
        public IList<DashboardCardItem> Itens { get; set; }
        public IList<DashboardUltimosExtratos> UltimosExtratos { get; set; }
    }
}
