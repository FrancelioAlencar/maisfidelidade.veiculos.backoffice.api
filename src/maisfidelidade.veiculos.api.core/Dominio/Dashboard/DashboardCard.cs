﻿using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.veiculos.backoffice.core.Dominio.Dashboard
{
    public class DashboardCard
    {
        public DateTime Mes { get; set; }
        public string MesExtenso { get; set; }
        public IList<DashboardCardItem> Itens { get; set; }        
    }

    public class DashboardCardItem
    {
        public TipoCardEnum Tipo { get; set; }
        public int Quantidade { get; set; }
        public string Mensagem { get; set; }
    }
}
