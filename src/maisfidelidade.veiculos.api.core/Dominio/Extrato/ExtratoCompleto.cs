﻿using maisfidelidade.veiculos.backoffice.core.Dominio.Dashboard;
using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.veiculos.backoffice.core.Dominio.Extrato
{
    public class ExtratoCompleto
    {
        public DateTime Mes { get; set; }
        public string MesExtenso { get; set; }
        public IList<DashboardCardItem> Cards { get; set; }
        public IList<DashboardUltimosExtratos> Semanais { get; set; }
        public DashboardUltimosExtratos Mensal { get; set; }
    }
}
