﻿using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.veiculos.backoffice.core.Dominio
{
    public class BeneficioResgatado
    {
        public string CNPJ { get; set; }
        public string Categoria { get; set; }
        public string Beneficio { get; set; }
        public int Porcentagem { get; set; }         
        public DateTime DataInclusao { get; set; }
        public bool AcaoSejaPlatinum { get; set; }
    }
}
