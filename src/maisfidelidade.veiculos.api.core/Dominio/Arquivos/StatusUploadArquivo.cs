﻿using MongoDB.Bson;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.veiculos.backoffice.core.Dominio
{
    public class StatusUploadArquivo
    {
        [JsonProperty("file_id")]
        public ObjectId? IdArquivo { get; set; }

        [JsonProperty("error")]
        public bool Erro { get; set; }

        [JsonProperty("message")]
        public string Mensagem { get; set; }
    }
}
