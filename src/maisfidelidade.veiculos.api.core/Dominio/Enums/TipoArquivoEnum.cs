﻿using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.veiculos.backoffice.core.Dominio.Enums
{
    public enum TipoArquivoEnum
    {
        Semanal = 1,
        Mensal = 2
    }
}
