﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.veiculos.backoffice.core.Dominio
{
    public class RelatorioResgateItemSolicitado
    {
        [JsonProperty("file_id")]
        public int IdArquivo { get; set; }

        [JsonProperty("month")]
        public int Mes { get; set; }

        [JsonProperty("year")]
        public int Ano { get; set; }
    }
}
