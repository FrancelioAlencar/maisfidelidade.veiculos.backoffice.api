﻿using maisfidelidade.veiculos.backoffice.core.Infrastructure;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Text;

namespace maisfidelidade.veiculos.backoffice.core.Dominio
{
    public interface IArquivoMongoRepositorio : IMongoRepositorio<Arquivo>
    {
        StatusProcessamentoArquivo ObterStatusAndamentoProcesso(int idUsuario);
        ObjectId? Salvar(Arquivo entidade);
        Arquivo Obter(ObjectId idArquivo);
        Arquivo ObterPorNome(string nome);
        IList<Arquivo> ObterLista();
        Arquivo ObterPorUsuario(int idUsuario);
    }
}
