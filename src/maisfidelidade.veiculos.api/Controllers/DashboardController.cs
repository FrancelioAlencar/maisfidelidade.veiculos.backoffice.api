﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Amazon.Lambda.APIGatewayEvents;
using Amazon.Lambda.TestUtilities;
using maisfidelidade.veiculos.backoffice.core.Dominio.Importacao;
using maisfidelidade.veiculos.backoffice.core.Dominio.Dashboard;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using maisfidelidade.veiculos.backoffice.core.Authorization;

namespace maisfidelidade.veiculos.backoffice.api.Controllers

{
    /// <summary>
    /// DashboardController
    /// </summary>
    [ApiAuthorize]
    [Produces("application/json")]
    [Route("api/dashboard")]
    [ApiController]
    public class DashboardController : ApiControllerBase
    {
        /// <summary>
        /// Obtém as informações do dashboard
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult ObterInformacoesDashboard()
        {
            try
            {
                var function = new dashboard.Function();
                var context = new TestLambdaContext();

                APIGatewayProxyRequest request = new APIGatewayProxyRequest
                {
                    Headers = new Dictionary<string, string>
                    {
                        { "IdUsuario", ObterIdUsuario().ToString() }
                    }
                };

                var resultado = function.FunctionHandler(request, context);

                if (resultado.StatusCode == 200)
                    return Ok(JsonConvert.DeserializeObject<DashboardCompleto>(resultado.Body, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore, MissingMemberHandling = MissingMemberHandling.Ignore }));

                return StatusCode(resultado.StatusCode);
            }
            catch (Exception e)
            {
                return Ok(e);
            }
        }

    }
}
