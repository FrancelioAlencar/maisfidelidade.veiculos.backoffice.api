﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace maisfidelidade.veiculos.backoffice.api.Controllers
{
    /// <summary>
    /// ApiControllerBase 
    /// </summary>
    public class ApiControllerBase : ControllerBase
    {
        /// <summary>
        /// ObterIdUsuario()
        /// </summary>
        /// <returns></returns>
        protected int ObterIdUsuario()
        {
            try
            {
                var claimsIndentity = this.User.Identity as ClaimsIdentity;

                var claim = claimsIndentity.Claims.First(c => c.Type == "IdUsuario");

                return int.Parse(claim.Value);
            }
            catch (Exception exception)
            {
                throw new Exception("Não consguiu obter o IdUsuario", exception);
            }
        }
    }
}
