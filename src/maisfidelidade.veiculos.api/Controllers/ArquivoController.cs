﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Amazon;
using Amazon.S3;
using Amazon.S3.Transfer;
using maisfidelidade.veiculos.backoffice.core.Authorization;
using maisfidelidade.veiculos.backoffice.core.Dominio;
using maisfidelidade.veiculos.backoffice.core.Dominio.Enums;
using maisfidelidade.veiculos.backoffice.core.Helpers;
using maisfidelidade.veiculos.backoffice.core.Log;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;

namespace maisfidelidade.veiculos.backoffice.api.Controllers
{
    /// <summary>
    /// ArquivoController
    /// </summary>
    [ApiAuthorize]
    [Route("api/arquivo")]
    [ApiController]
    public class ArquivoController : ApiControllerBase
    {
        private readonly IArquivoMongoRepositorio arquivoMongoRepositorio;
        private readonly ILogMongoRepositorio logMongoRepositorio;

        /// <summary>
        /// Construtor de ArquivoController
        /// </summary>
        /// <param name="arquivoMongoRepositorio"></param>
        /// <param name="logMongoRepositorio"></param>
        public ArquivoController(IArquivoMongoRepositorio arquivoMongoRepositorio, ILogMongoRepositorio logMongoRepositorio)
        {
            this.arquivoMongoRepositorio = arquivoMongoRepositorio;
            this.logMongoRepositorio = logMongoRepositorio;
        }

        /// <summary>
        /// Realiza a importação do arquivo
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        [HttpPost("upload")]
        public async Task<IActionResult> ImportarArquivoParaProcessamentoNoS3([FromForm]IFormFile file)
        {
            try
            {
                int idUsuario = ObterIdUsuario();

                if (idUsuario == 0)
                {
                    return Ok(new StatusUploadArquivo
                    {
                        IdArquivo = null,
                        Erro = true,
                        Mensagem = "idUsuario é necessário."
                    });
                }

                if (file == null)
                {
                    return Ok(new StatusUploadArquivo
                    {
                        IdArquivo = null,
                        Erro = true,
                        Mensagem = "Arquivo é necessário."
                    });
                }

                Arquivo arquivo = arquivoMongoRepositorio.ObterPorUsuario(idUsuario);

                if (arquivo != null)
                {
                    if ((arquivo.StatusArquivo == StatusArquivoEnum.Pendente) ||
                        (arquivo.StatusArquivo == StatusArquivoEnum.Processando) ||
                        (arquivo.StatusArquivo == StatusArquivoEnum.ProcessadoComSucesso) ||
                        (arquivo.StatusArquivo == StatusArquivoEnum.AguardandoAprovacao) ||
                        (arquivo.StatusArquivo == StatusArquivoEnum.Publicando))
                    {
                        return Ok(new StatusUploadArquivo
                        {
                            IdArquivo = arquivo.Id,
                            Erro = true,
                            Mensagem = "Existe um arquivo que ainda não foi concluído"
                        });
                    }
                    else
                    {
                        if ((arquivo.StatusArquivo == StatusArquivoEnum.ErroAoProcessar) ||
                            (arquivo.StatusArquivo == StatusArquivoEnum.ErroOrquestracao) ||
                            (arquivo.StatusArquivo == StatusArquivoEnum.ErroAoPublicar))
                        {
                            // reprocessamento de arquivo
                            arquivo.StatusArquivo = StatusArquivoEnum.Cancelado;
                            arquivo.StatusAndamentoProcesso = StatusAndamentoProcessoEnum.Cancelado;
                            arquivo.Nome = "erro-" + DateTime.Now.ToString("HHmmss") + "-" + arquivo.Nome;

                            var logInfo = new LogInfo();
                            logInfo.DataInicio = DateTime.Now;
                            logInfo.DataFim = DateTime.Now;
                            logInfo.IdArquivo = arquivo.Id;
                            logInfo.Mensagem = "Arquivo cancelado para novo envio";
                            logInfo.StatusAndamentoProcesso = StatusAndamentoProcessoEnum.Cancelado;
                            logInfo.StatusLog = StatusLogEnum.CanceladoParaReenvio;
                            logInfo.TempoDecorrido = new TimeSpan(0, 0, 0);
                            logInfo.Tipo = TipoLog.Log;

                            logMongoRepositorio.Salvar(logInfo);
                            arquivoMongoRepositorio.Salvar(arquivo);
                        }
                    }
                }

                var arquivoExiste = arquivoMongoRepositorio.ObterPorNome(file.FileName);
                if (arquivoExiste != null)
                {
                    if (arquivoExiste.StatusArquivo != StatusArquivoEnum.Cancelado &&
                        arquivoExiste.StatusAndamentoProcesso != StatusAndamentoProcessoEnum.Cancelado)
                    {
                        return Ok(new StatusUploadArquivo
                        {
                            IdArquivo = arquivoExiste.Id,
                            Erro = true,
                            Mensagem = "Este nome de arquivo já foi utilizado."
                        });
                    }
                }

                using (var amazonS3Client = new AmazonS3Client(RegionEndpoint.SAEast1))
                {
                    using (var fileTransferUtility = new TransferUtility(amazonS3Client))
                    {
                        string bucketName = "opah-maisfidelidade";
                        string keyName = "arquivo-atualizacao-desconto/hml";

                        using (var memoryStream = new MemoryStream())
                        {
                            file.CopyTo(memoryStream);

                            var fileTransferUtilityRequest = new TransferUtilityUploadRequest
                            {
                                BucketName = string.Format("{0}/{1}", bucketName, keyName),
                                Key = file.FileName,
                                InputStream = memoryStream,
                                CannedACL = S3CannedACL.PublicRead
                            };

                            await fileTransferUtility.UploadAsync(fileTransferUtilityRequest);

                            ObjectId? idArquivo = arquivoMongoRepositorio.Salvar(new Arquivo(file.FileName, DateTime.Now, null, null, null, null, StatusAndamentoProcessoEnum.UploadDoArquivo, 0, idUsuario));

                            if (idArquivo == null)
                            {
                                return Ok(new StatusUploadArquivo
                                {
                                    IdArquivo = idArquivo,
                                    Erro = true,
                                    Mensagem = $"Ocorreu alguma falha ao registrar na base o arquivo enviado"
                                });
                            }

                            return Ok(new StatusUploadArquivo
                            {
                                IdArquivo = idArquivo,
                                Erro = false,
                                Mensagem = null
                            });
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /// <summary>
        /// Obtém o status do andamento da importação do arquivo
        /// </summary>
        /// <returns></returns>
        [HttpGet("upload/status")]
        public IActionResult ObterStatusAndamentoImportacao()
        {
            try
            {
                int idUsuario = ObterIdUsuario();

                var status = arquivoMongoRepositorio.ObterStatusAndamentoProcesso(idUsuario);
                
                if (status == null)
                {
                    return Ok(new { Mensagem = "Não foi encontrado nenhum arquivo sendo processado" });
                } 

                return Ok(status);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /// <summary>
        /// Realiza a publicação do arquivo
        /// </summary>
        /// <param name="idArquivo"></param>
        /// <returns></returns>
        [HttpPost("publicacao")]
        public IActionResult PublicarArquivoImportado([FromQuery] string idArquivo)
        {
            try
            {
                var arquivo = arquivoMongoRepositorio.Obter(ObjectId.Parse(idArquivo));

                if (arquivo.StatusArquivo == StatusArquivoEnum.AguardandoAprovacao &&
                    arquivo.StatusAndamentoProcesso == StatusAndamentoProcessoEnum.AguardandoPublicacao)
                {
                    AWSHelper.EnviarSNS(idArquivo, "sns-publicacao");

                    return Ok("Arquivo enviado para publicação");
                }
                else
                {
                    return Ok("Arquivo não está aguardando validação");
                }
            }
            catch (Exception exception)
            {
                logMongoRepositorio.Salvar(new LogInfo { IdArquivo = null, Mensagem = idArquivo + "|" + exception.Message });

                throw new Exception(exception.Message);
            }
        }

    }
}
