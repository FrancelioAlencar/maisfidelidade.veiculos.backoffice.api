﻿using Microsoft.AspNetCore.Mvc;

namespace maisfidelidade.veiculos.api.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class HomeController : ControllerBase
	{
		[HttpGet]
		public string Get()
		{
			return "Welcome to Mais Fidelidade Veículos api";
		}
	}
}
