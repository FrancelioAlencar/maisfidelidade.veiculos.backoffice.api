﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Amazon.Lambda.APIGatewayEvents;
using Amazon.Lambda.TestUtilities;
using maisfidelidade.veiculos.backoffice.core.Dominio.Importacao;
using maisfidelidade.veiculos.backoffice.core.Dominio.Dashboard;
using maisfidelidade.veiculos.backoffice.core.Dominio.Extrato;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using maisfidelidade.veiculos.backoffice.core.Authorization;

namespace maisfidelidade.veiculos.backoffice.api.Controllers

{
    /// <summary>
    /// ExtratoController
    /// </summary>
    [ApiAuthorize]
    [Produces("application/json")]
    [Route("api/extrato")]
    [ApiController]
    public class ExtratoController : ApiControllerBase
    {
        /// <summary>
        /// Obtém as informações dos extratos
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult ObterRelatorioExtratos()
        {
            try
            {
                var function = new extrato.Function();
                var context = new TestLambdaContext();

                APIGatewayProxyRequest request = new APIGatewayProxyRequest
                {
                    Headers = new Dictionary<string, string>
                    {
                        { "IdUsuario", ObterIdUsuario().ToString() }
                    }
                };

                var resultado = function.FunctionHandler(request, context);

                if (resultado.StatusCode == 200)
                    return Ok(JsonConvert.DeserializeObject<IList<ExtratoCompleto>>(resultado.Body, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore, MissingMemberHandling = MissingMemberHandling.Ignore }));

                return StatusCode(resultado.StatusCode);
            }
            catch (Exception e)
            {
                return Ok(e);
            }
        }
    }
}
