﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amazon.Lambda.APIGatewayEvents;
using Amazon.Lambda.TestUtilities;
using maisfidelidade.veiculos.backoffice.core.Authorization;
using maisfidelidade.veiculos.backoffice.core.Dominio;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;


namespace maisfidelidade.veiculos.backoffice.api.Controllers
{
    /// <summary>
    /// ResgateController
    /// </summary>
    [ApiAuthorize]
    [Route("api/resgate")]
    [ApiController]
    public class ResgateController : ApiControllerBase
    {
        /// <summary>
        /// Obtém as informações dos resgates
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult ObterRelatorioResgates()
        {
            try
            {
                var function = new relatorioresgates.Function();

                APIGatewayProxyRequest request = new APIGatewayProxyRequest
                {
                    Headers = new Dictionary<string, string>
                    {
                        { "IdUsuario", ObterIdUsuario().ToString() }
                    }
                };

                var resultado = function.FunctionHandler(request, new TestLambdaContext());

                return Ok(JsonConvert.DeserializeObject<RelatorioResgate>(resultado.Body, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore, MissingMemberHandling = MissingMemberHandling.Ignore }));
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /// <summary>
        /// Realiza o download do arquivo .csv com as informações do resgate mensal
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPost("download")]
        public IActionResult FazerDownloadArquivoCsvResgateMensal([FromBody] RelatorioResgateItemSolicitado item)
        {
            try
            {
                var function = new resgate.Function();

                var resultado = function.FunctionHandler(new APIGatewayProxyRequest()
                {
                    Headers = new Dictionary<string, string>
                    {
                        { "IdUsuario", ObterIdUsuario().ToString() },
                        { "IdArquivo", item.IdArquivo.ToString() },
                        { "Mes", item.Mes.ToString() },
                        { "Ano", item.Ano.ToString() }
                    }
                }, new TestLambdaContext());

                var resgates = JsonConvert.DeserializeObject<IList<BeneficioResgatado>>(resultado.Body, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore, MissingMemberHandling = MissingMemberHandling.Ignore });

                var columnHeaders = new string[]
                {
                    "CNPJ",
                    "Categoria",
                    "Beneficio",
                    "Porcentagem",
                    "Data Inclusao",
                    "Acao Seja Platinum"
                };

                var resgateRecords = (from resgate in resgates
                                      select new object[]
                                      {
                                          resgate.CNPJ,
                                          $"{resgate.Categoria}",
                                          resgate.Beneficio,                                          
                                          resgate.Porcentagem,
                                          resgate.DataInclusao.ToString("dd/MM/yyyy"),
                                          resgate.AcaoSejaPlatinum,
                                      }).ToList();

                var resgateCsv = new StringBuilder();
                
                resgateRecords.ForEach(line =>
                {
                    resgateCsv.AppendLine(string.Join(";", line));
                });

                byte[] buffer = Encoding.ASCII.GetBytes($"{string.Join(";", columnHeaders)}\r\n{resgateCsv.ToString()}");
                
                return File(buffer, "text/csv", $"Resgates.csv");
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

    }
}
