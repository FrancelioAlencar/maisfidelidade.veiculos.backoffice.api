﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using maisfidelidade.veiculos.backoffice.core.Infrastructure.Data;
using maisfidelidade.veiculos.backoffice.core.Infrastructure.Data.MySql;
using maisfidelidade.veiculos.backoffice.core.Configuration;
using maisfidelidade.veiculos.backoffice.core.Infrastructure.Data.Sql;
using maisfidelidade.veiculos.backoffice.core.Dominio;
using maisfidelidade.veiculos.backoffice.core.Infrastructure.Repositorios.MongoDB;
using maisfidelidade.veiculos.backoffice.core.Infrastructure.Data.MongoDB;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using System.Text;
using maisfidelidade.veiculos.backoffice.core.Infrastructure.Repositorios;

namespace maisfidelidade.veiculos.backoffice.api
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			DatabaseFactory.SetDatabases(GetDatabase);
			MongoDatabaseFactory.SetDatabases(GetMongoDatabase);

			services.AddSingleton<IArquivoMongoRepositorio, ArquivoMongoRepositorio>();
			services.AddSingleton<ILogMongoRepositorio, LogMongoRepositorio>();

			services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

			services.AddCors(options =>
			{
				options.AddPolicy("CorsPolicy", //Allow Cross origin
					builder => builder.AllowAnyOrigin()
					.AllowAnyMethod()
					.AllowAnyHeader()
					.AllowCredentials());
			});

			services.AddSwaggerGen(c =>
			{
				c.SwaggerDoc("v1",
					new OpenApiInfo
					{
						Title = "Documentação da API",
						Version = "v1",
						Description = "Está API é ultilizada para o sistema Mais Fidelidade Veículos",
						Contact = new OpenApiContact
						{
							Name = "Opah"
						}
					});

				var caminhoAplicacao = AppContext.BaseDirectory;
				var nomeAplicacao = System.Reflection.Assembly.GetEntryAssembly().GetName().Name;
				var caminhoXmlDoc = Path.Combine(caminhoAplicacao, $"{nomeAplicacao}.xml");

				c.IncludeXmlComments(caminhoXmlDoc);
			});

			ConfigurarAutenticacao(services);

			//services
			//	.AddHealthChecks()
			//	.AddCheck("Welcome", () =>
			//	{
			//		return HealthCheckResult.Healthy("Welcome to Mais Fidelidade Veículos api");
			//	});
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env)
		{
			app.UseCors("CorsPolicy");

			//if (env.IsDevelopment())
			//{
			//	app.UseDeveloperExceptionPage();
			//}
			//else
			//{
			//	The default HSTS value is 30 days.You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
			//	app.UseHsts();
			//}

			app.UseAuthentication();
			app.UseHttpsRedirection();
			app.UseMvc();			

			app.UseSwagger();
			app.UseSwaggerUI(c =>
			  {
				  c.SwaggerEndpoint("/swagger/v1/swagger.json", "Api");
				  c.RoutePrefix = "doc";
			  });

			//app.UseHealthChecks("/Healthcheck", new HealthCheckOptions());
		}



		#region Métodos Privados

		private void ConfigurarAutenticacao(IServiceCollection services)
		{
			var securityKey = ConfigurationManager.AppSettings["jwt:key"];

			if (string.IsNullOrWhiteSpace(securityKey))
			{
				throw new ArgumentException("SecurityKey está vaiza ou nula", nameof(securityKey));
			}

			var sessionValue = ConfigurationManager.AppSettings["tempoSessao"];

			if (!int.TryParse(sessionValue, out int sessionTime))
			{
				sessionTime = 60;
			}

			services.AddAuthentication(options =>
			{
				options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
				options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
			}).AddJwtBearer(options =>
			{
				options.RequireHttpsMetadata = false;
				options.SaveToken = true;
				options.TokenValidationParameters = new TokenValidationParameters
				{
					ValidateAudience = false,
					ValidateIssuer = false,
					ValidateLifetime = false,
					ValidateIssuerSigningKey = true,
					IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(securityKey)),
					ClockSkew = TimeSpan.Zero
				};
			});

			services.AddAuthorization(auth =>
			{
				auth.AddPolicy("Bearer", new AuthorizationPolicyBuilder()
					.AddAuthenticationSchemes(JwtBearerDefaults.AuthenticationScheme‌​)
					.RequireAuthenticatedUser().Build());
			});

		}

		private static Database GetDatabase(string name)
		{
			try
			{
				Database database = null;

				switch (name)
				{
					case "read":
						database = new MySqlDatabase(new DbSettings("connection_maisfidelidade_mysql_read").ConnectionString());
						break;
					case "write":
						database = new MySqlDatabase(new DbSettings("connection_maisfidelidade_mysql_write").ConnectionString());
						break;
					case "webmotors":
						database = new SqlDatabase(new DbSettings("connection_maisfidelidade_sqlserver").ConnectionString());
						break;
				}

				if (database == null)
				{
					throw new ArgumentNullException("");
				}

				return database;
			}
			catch (Exception exception)
			{
				throw new Exception("Erro ao instanciar a conexão", exception);
			}
		}

		private static MongoDatabase GetMongoDatabase(string name)
		{
			try
			{
				MongoDatabase database = null;

				switch (name)
				{
					case "mongo":
						database = new MongoDatabase(new DbSettings("connection_maisfidelidade_mongodb").ConnectionString());
						break;
				}

				if (database == null)
				{
					throw new ArgumentNullException("");
				}

				return database;
			}
			catch (Exception exception)
			{
				throw new Exception("Erro ao instanciar a conexão", exception);
			}
		}
		
		#endregion
	}
}
