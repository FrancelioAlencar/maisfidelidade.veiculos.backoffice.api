using Amazon.Lambda.APIGatewayEvents;
using Amazon.Lambda.Core;
using maisfidelidade.veiculos.backoffice.core.DI;
using maisfidelidade.veiculos.backoffice.core.Dominio;
using maisfidelidade.veiculos.backoffice.core.Dominio.Importacao;
using maisfidelidade.veiculos.backoffice.core.Dominio.Dashboard;
using maisfidelidade.veiculos.backoffice.core.Dominio.Extrato;
using maisfidelidade.veiculos.backoffice.core.Infrastructure.Repositorios;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using maisfidelidade.veiculos.backoffice.core.Infrastructure;

[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace maisfidelidade.veiculos.backoffice.api.extrato
{
    public class Function
    {
        //private readonly IArquivoRepositorio arquivoRepositorio;
        private readonly IArquivoMongoRepositorio arquivoRepositorio;
        private readonly ILogMongoRepositorio logMongoRepositorio;

        public Function()
        {
            var resolvedorDependencia = new ResolvedorDependencia();

            arquivoRepositorio = resolvedorDependencia.ObterServico<IArquivoMongoRepositorio>();
            logMongoRepositorio = resolvedorDependencia.ObterServico<ILogMongoRepositorio>();
        }

        public APIGatewayProxyResponse FunctionHandler(APIGatewayProxyRequest request, ILambdaContext _lambdaContext)
        {
            try
            {
                var arquivos = arquivoRepositorio.ObterLista();

                var extratos = new List<ExtratoCompleto>();

                foreach (var arquivo in arquivos.GroupBy(x => Convert.ToDateTime(x.DataReferencia).ToString("MM/yyyy")))
                {
                    if (extratos.Count < 3)
                    {
                        ExtratoCompleto extrato = new ExtratoCompleto();

                        extrato.Cards = new List<DashboardCardItem>();
                        extrato.Semanais = new List<DashboardUltimosExtratos>();

                        extrato.Cards.Add(new DashboardCardItem
                        {
                            Tipo = TipoCardEnum.RegistrosProcessados,
                            Quantidade = arquivo.Sum(x => x.QtdLinhasProcessadas),
                            Mensagem = "Registros processados no m�s."
                        });                        

                        extrato.Cards.Add(new DashboardCardItem
                        {
                            Tipo = TipoCardEnum.ArquivosProcessados,
                            Quantidade = arquivo.Count(),
                            Mensagem = "Arquivos processados no m�s."
                        });

                        extrato.Mes = Convert.ToDateTime(arquivo.First().DataReferencia);
                        extrato.MesExtenso = Convert.ToDateTime(arquivo.First().DataReferencia).ToString("MMMM");

                        int erros = 0;

                        foreach (var item in arquivo)
                        {
                            var log = logMongoRepositorio.Obter(item.Id);

                            var logErros = log.Where(x => x.Tipo == core.Log.TipoLog.Falha).GroupBy(x => x.StatusLog).ToList();

                            erros += logErros.Count;

                            if ((TipoArquivoEnum)item.TipoArquivo == TipoArquivoEnum.Semanal)
                            {
                                extrato.Semanais.Add(new DashboardUltimosExtratos
                                {
                                    DataReferencia = Convert.ToDateTime(item.DataReferencia),
                                    Data = Convert.ToDateTime(item.DataReferencia).ToString("dd/MMM"),
                                    IdArquivo = item.Id.ToString(),
                                    LinkArquivo = item.LinkArquivo,
                                    LinkExtrato = item.LinkExtrato,
                                    NomeArquivo = item.Nome,
                                    QuantidadeLinhas = item.QtdLinhasProcessadas,
                                    TipoArquivo = (TipoArquivoEnum)item.TipoArquivo
                                });
                            }
                            else
                            {
                                extrato.Mensal = new DashboardUltimosExtratos
                                {
                                    DataReferencia = Convert.ToDateTime(item.DataReferencia),
                                    Data = Convert.ToDateTime(item.DataReferencia).ToString("dd/MMM"),
                                    IdArquivo = item.Id.ToString(),
                                    LinkArquivo = item.LinkArquivo,
                                    LinkExtrato = item.LinkExtrato,
                                    NomeArquivo = item.Nome,
                                    QuantidadeLinhas = item.QtdLinhasProcessadas,
                                    TipoArquivo = (TipoArquivoEnum)item.TipoArquivo,
                                    Publicado = item.StatusArquivo == core.Dominio.Enums.StatusArquivoEnum.PublicadoComSucesso ? true : false
                                };
                            }
                        }

                        extrato.Cards.Add(new DashboardCardItem
                        {
                            Tipo = TipoCardEnum.RegistrosComErros,
                            Quantidade = erros,
                            Mensagem = "Erros encontrados nos arquivos enviados."//registros processados."
                        });

                        extratos.Add(extrato);
                    }
                    else { break; }
                }

                return new APIGatewayProxyResponse()
                {
                    StatusCode = (int)HttpStatusCode.OK,
                    Headers = new Dictionary<string, string> { { "Access-Control-Allow-Origin", "*" }, { "Access-Control-Allow-Headers", "*" } },
                    Body = JsonConvert.SerializeObject(extratos, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore, MissingMemberHandling = MissingMemberHandling.Ignore })
                };
            }
            catch (Exception _exception)
            {
                _exception.Data.Add("LambdaContext", JsonConvert.SerializeObject(_lambdaContext));
                return null;
            }
        }
    }
}
