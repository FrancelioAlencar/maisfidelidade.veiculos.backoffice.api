using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Amazon.Lambda.APIGatewayEvents;
using Amazon.Lambda.Core;
using maisfidelidade.veiculos.backoffice.core.DI;
using maisfidelidade.veiculos.backoffice.core.Dominio;
using Newtonsoft.Json;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace maisfidelidade.veiculos.backoffice.resgate
{
    public class Function
    {
        private readonly IResgateRepositorio resgateRepositorio;

        public Function()
        {
            var resolvedorDependencia = new ResolvedorDependencia();

            resgateRepositorio = resolvedorDependencia.ObterServico<IResgateRepositorio>();
        }


        /// <summary>
        /// A simple function that takes a string and does a ToUpper
        /// </summary>
        /// <param name="input"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public APIGatewayProxyResponse FunctionHandler(APIGatewayProxyRequest request, ILambdaContext context)
        {
            try
            {
                if (!int.TryParse(request.Headers["IdArquivo"], out int idAquivo))
                {
                    throw new ArgumentException("Id do Arquivo inv�lido", nameof(idAquivo));
                }

                if (!int.TryParse(request.Headers["Mes"], out int mes))
                {
                    throw new ArgumentException("M�s inv�lido", nameof(mes));
                }

                if (!int.TryParse(request.Headers["Ano"], out int ano))
                {
                    throw new ArgumentException("Ano inv�lido", nameof(ano));
                }

                var dataInicio = new DateTime(ano, mes, 1, 0, 0, 0);
                var dataFim = new DateTime(ano, mes, 1, 0, 0, 0).AddMonths(1).AddHours(23).AddMinutes(59).AddSeconds(59).AddDays(-1);

                var resgates = resgateRepositorio.ObterResgate(idAquivo, dataInicio, dataFim);

                return new APIGatewayProxyResponse()
                {
                    StatusCode = (int)HttpStatusCode.OK,
                    Headers = new Dictionary<string, string> { { "Access-Control-Allow-Origin", "*" }, { "Access-Control-Allow-Headers", "*" } },
                    Body = JsonConvert.SerializeObject(resgates, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore, MissingMemberHandling = MissingMemberHandling.Ignore })
                };
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
    }
}
